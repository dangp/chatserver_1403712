/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatserver;

/**
 *
 * @author HaiPhan
 */
public interface HistoryObserver {
    void newChatEntry(String e);
}
