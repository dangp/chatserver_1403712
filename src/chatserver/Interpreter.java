/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatserver;

import java.io.PrintWriter;
import static java.lang.System.out;
import java.net.Socket;
import java.util.Scanner;

/**
 *
 * @author HaiPhan
 */
public class Interpreter implements HistoryObserver, Runnable {

    private Socket socket;
    private History history;
    private String username;

    public Interpreter(Socket s, History history) {
        this.socket = s;
        this.history = history;
    }

    @Override
    public void run() {

        try {
            Scanner in = new Scanner(socket.getInputStream());
            PrintWriter out = new PrintWriter(socket.getOutputStream());
            out.println("Type @history to print the chat history.");
            out.println("Type your name: ");
            out.flush();
            username = in.nextLine();
            history.add(username + " has entered the chatroom.");

            while (true) {
                if (in.hasNext()) {
                    String input = in.nextLine();
                    if (input.equals("@history")) {
                        this.printHistory();
                    } else {
                        history.add("<" + username + ">: " + input);
                        System.out.println("<" + username + ">: " + input);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void newChatEntry(String a) {
        try {
            PrintWriter out = new PrintWriter(socket.getOutputStream());
            out.println(a);
            out.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void printHistory() {
        try {
            PrintWriter out = new PrintWriter(socket.getOutputStream());
            out.println("Chat History: ");
            for (String i : history.getChatHistory()) {
                out.println(i);
                out.flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
