/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatserver;

import java.util.ArrayList;

/**
 *
 * @author HaiPhan
 */
public class History {

    private ArrayList<HistoryObserver> historyObserver;
    private ArrayList<String> chatHistory;

    private History() {
        chatHistory = new ArrayList<String>();
        historyObserver = new ArrayList<HistoryObserver>();
    }

    private static final History instance = new History();

    public static History getInstance() {
        return instance;
    }

    public void add(String e) {
        this.chatHistory.add(e);
        notifyObservers(e);
    }

    public ArrayList<String> getChatHistory()   {
        return this.chatHistory;
    }

    //@Override
    public void addObserver(HistoryObserver observer) {
        historyObserver.add(observer); //To change body of generated methods, choose Tools | Templates.
    }

    //@Override
    public void notifyObservers(String e) {
        for (HistoryObserver i : historyObserver) {
                i.newChatEntry(e);
            //To change body of generated methods, choose Tools | Templates.
        }

    }
}
