/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatserver;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author HaiPhan
 */
public class Main {

    public static void main(String[] args) throws IOException {
        try {
            int PORT = 12345;
            ServerSocket server = new ServerSocket(PORT);
            System.out.println("Waiting for clients...");
            while (true) {
                Socket s = server.accept();
                System.out.println("Client connected from " + s.getLocalAddress().getHostName());
                Interpreter chat = new Interpreter(s, History.getInstance());
                History.getInstance().addObserver(chat);
                Thread t = new Thread(chat);
                t.start();
            }
        } catch (Exception e) {
            System.out.println("Error!!!");
            e.printStackTrace();
        }
    }
}
